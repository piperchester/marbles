'use strict';
 

/**
 * @ngdoc function
 * @name marblesApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the marblesApp
 */
angular.module('marblesApp', ['firebase'])
  .controller('MainCtrl', ['$scope', '$firebase', function($scope, $firebase) {

    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

	var ref = new Firebase('https://av1pm2jbus8.firebaseio-demo.com/');
    $scope.messages = $firebase(ref).$asArray();

    //ADD MESSAGE METHOD
          $scope.addMessage = function(e) {

            //LISTEN FOR RETURN KEY
            if (e.keyCode === 13 && $scope.msg) {
              //ALLOW CUSTOM OR ANONYMOUS USER NAMES
              var name = $scope.name || 'anonymous';
          $scope.messages.$add({from: name, body: $scope.msg});
              //RESET MESSAGE
              $scope.msg = '';
            }
          };

    

  }]);
