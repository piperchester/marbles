'use strict';

/**
 * @ngdoc function
 * @name marblesApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the marblesApp
 */
angular.module('marblesApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
